/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.filter;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 *
 * @author operador
 */
public class UrlFilter {
    
  private ArrayList<Pattern> include = new ArrayList<>();
  private ArrayList<Pattern> exclude = new ArrayList<>();

  public UrlFilter()
  {

  }
  public UrlFilter include(String pattern)
  {
    include.add(generateExpression(pattern));
    return this;
  }
  public UrlFilter exclude(String pattern)
  {
    exclude.add(generateExpression(pattern));
    return this;
  }
  public boolean matches(String uri)
  {
    boolean match = false;

    //check inclusions
    for (Pattern pattern : include)
    {
      match = match || pattern.matcher(uri).matches();
    }

    if (!match)
      return false;

    //check exclusions
    for (Pattern pattern : exclude)
    {
      match = match && !pattern.matcher(uri).matches();
    }
    return match;
  }
  
  /** regular expression special character */
  private static char[] specialChars = { '[', '\\', '^', '$', '.', '|', '?', '*', '+', '(', ')' };

  private static Pattern generateExpression(String input)
  {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < input.length(); i++)
    {
      char letter = input.charAt(i);
      if (letter == '*')
      {
        sb.append(".*");
      }
      else if (contains(specialChars, letter))
      {
        sb.append("\\" + letter);
      }
      else
      {
        sb.append(letter);
      }
    }
    return Pattern.compile(sb.toString());
  }

  private static boolean contains(char[] array, char value)
  {
    if (array == null || array.length == 0)
    {
      return false;
    }

    for (int i = 0; i < array.length; i++)
    {
      char o = array[i];
      if (o == value)
      {
        return true;
      }
    }

    return false;
  }
}
