/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.controllers;

import br.com.models.Companies;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author operador
 */
@ManagedBean(name="companiesController")
@ViewScoped
public class CompaniesController {
    private List<Companies> companies = new ArrayList<>();
    public CompaniesController(){
        for(int i = 0; i< 20; i++){
            companies.add(generateRandomCompanies());
        }
    }
    public String [] nomes = {"Smartbr", "EvoCont", "Focus", "Isp"};
    public Companies generateRandomCompanies(){
        int indice = (int) Math.floor(Math.random()*4);
        Companies company = new Companies();
        company.setName(nomes[indice]);
        company.setCnpj(Math.random()*8);
        company.setCreated_at(new Date());
        return company;
    }

    public List<Companies> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Companies> companies) {
        this.companies = companies;
    }
    
}
