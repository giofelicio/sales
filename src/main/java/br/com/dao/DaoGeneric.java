/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dao;

import br.com.dao.connection.ConnectionFactory;
import javax.persistence.EntityManager;

/**
 *
 * @author operador
 * @param <T>
 */
public class DaoGeneric<T> {
       
	private static final EntityManager manager = ConnectionFactory.getEntityManager();
        
	public T findById(Class<T> clazz, Long id){
		return manager.find(clazz, id);
	}
	
	public void save(T obj){
            try{
                manager.getTransaction().begin();
                manager.persist(obj);
                manager.getTransaction().commit();
                manager.close();
            }catch(Exception e){
                System.out.println(e);
                manager.getTransaction().rollback();
            }
	}
	
	public void remove(Class<T> clazz, Long id){
            T t = findById(clazz, id);
            try{
                    manager.getTransaction().begin();
                    manager.remove(t);
                    manager.getTransaction().commit();
            }catch (Exception e) {
                    manager.getTransaction().rollback();
            }
	}
}
