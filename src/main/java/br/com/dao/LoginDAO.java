/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dao;

import br.com.dao.connection.ConnectionFactory;
import br.com.models.User;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author operador
 */
public class LoginDAO {
    private static final EntityManager manager = ConnectionFactory.getEntityManager();

    public LoginDAO() {
    }
    
    public static User validate(String login, String password) {
        User result;
        try {
            TypedQuery<User> query = manager.createNamedQuery("User.Login", User.class);
            query.setParameter("param", login);
            query.setParameter("password", password);
            result = query.getSingleResult();
            return result;
        } catch (Exception ex) {
            return null;
        } finally {
            manager.close();
        }
}
}
