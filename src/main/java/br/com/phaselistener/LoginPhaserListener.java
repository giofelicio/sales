package br.com.phaselistener;

import br.com.filter.UrlFilter;
import br.com.models.User;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

public class LoginPhaserListener implements PhaseListener {
    
    private FacesContext facesContext;
    
    @Override
    public void afterPhase(PhaseEvent event) {
        facesContext = event.getFacesContext();
        String viewId = facesContext.getViewRoot().getViewId();
        System.out.println(viewId.startsWith("http://"));
//if (viewId.contains("/uteis/")) {
//                    return;
//                }
        NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
        boolean paginaLogin = (viewId.lastIndexOf("login") > -1);
        
        if (existUserLogged() && paginaLogin) {
            nh.handleNavigation(facesContext, null, "/index?faces-redirect=true");
        } 
        else if (!existUserLogged() && !paginaLogin) {
            nh.handleNavigation(facesContext, null, "/login?faces-redirect=true");
        }
    }
    
    public boolean existUserLogged() {
        return (((User) getAtributteSession("user")) != null);
    }

    public Object getAtributteSession(String attributeName) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session != null) {
            return session.getAttribute(attributeName);
        }
        return null;
    }
    @Override
    public void beforePhase(PhaseEvent event) {
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
    
}
