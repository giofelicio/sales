package br.com.models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="users")
@NamedQueries({
    @NamedQuery(name= "User.FindParams", 
            query="SELECT u FROM User u WHERE u.cpf = :param OR u.email = :param OR u.login = :param"),
    @NamedQuery(name= "User.Login",
            query="SELECT u FROM User u WHERE (u.cpf = :param OR u.email = :param) AND (u.password = :password)"),
    @NamedQuery(name= "User.FinAll",
            query="SELECT u FROM User u")
})
public class User implements Serializable, Comparable<User>{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(name="name", length=50, nullable=false)
    private String name;
    @Column(name="cpf", length=11, nullable=false, unique=true)
    private String cpf;
    @Column(name="email", length=30, nullable=false, unique=true)
    private String email;
    @Column(name="login", length=20, nullable=false, unique=true)
    private String login;
    @Column(name="password", length=50, nullable=false)
    private String password;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Calendar created_at;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Calendar updated_at;

    public User() {
    }

//    public User(String nome, String cpf, String email, String log, String passd){
//        this.name = nome;
//        this.cpf = cpf;
//        this.email = email;
//        this.login = log;
//        this.password = new LoginUtil().MD5(passd);
//    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Calendar getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Calendar created_at) {
        this.created_at = created_at;
    }

    public Calendar getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Calendar updated_at) {
        this.updated_at = updated_at;
    }

    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.login);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(User o) {
        if (o.getId() > this.id) {
		return 1;
	} else if (o.getId() < this.id){
		return -1;
	} else {
		return 0;
	}
    }
}
