package br.com.models;

import java.util.Date;

/**
 *
 * @author operador
 */
public class Companies {

    private Long id;
    private String name;
    private Double cnpj;
    private Date created_at;
    
    public Companies(){
        
    }
    public Companies(String name, Double cnpj){
        super();
        this.name = name;
        this.cnpj = cnpj;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCnpj() {
        return cnpj;
    }

    public void setCnpj(Double cnpj) {
        this.cnpj = cnpj;
    }
    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
