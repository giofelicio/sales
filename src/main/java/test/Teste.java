
package test;

//import br.com.dao.DaoGeneric;
//import br.com.dao.connection.ConnectionFactory;
import br.com.dao.DaoGeneric;
import br.com.dao.connection.ConnectionFactory;
import br.com.models.User;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.HttpResponse;
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.primefaces.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author PC
 */
public class Teste {
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException{
//            Pattern pa = Pattern.compile("a*b");
//            System.out.println(pa);
//            DaoGeneric<User> daoG = new DaoGeneric<>();
//        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
//            EntityManager manager = ConnectionFactory.getEntityManager();
//            User user = new User();
//            user.setName("Giovanni");
//            user.setCpf("70232615160");
//            user.setEmail("giovanni@smartbr.com");
//            user.setLogin("giovannifc");
//            user.setPassword("09112013");
//            daoG.save(user);
//          User result;
//          TypedQuery<User> query = manager.createNamedQuery("User.Login", User.class);
//          query.setParameter("param", "70232615160");
//          query.setParameter("password", "09112013");
//            try {
//                 result = query.getSingleResult();
//            } 
//            catch (Exception e) {
//                 result = null;
//            }
//          System.out.println(query);
//        DaoGeneric<User> daoUser = new DaoGeneric<>();
//        LoginUtil loginUtil  = new LoginUtil();
//        EntityManager manager = ConnectionFactory.getEntityManager();
//        List<User> list = null;
//        try{
//            list = manager.createQuery("from User u").getResultList();
//        }
//        catch(Exception e){
//            System.err.println(e);
//        }
//        finally {
//            manager.close();
//        }
//        
//        for(User u: list){
//            System.out.println(u.getName());
//        }
    /*
    //Aggregate function
    Query query1;
    query1 = manager.
    createQuery("Select MAX(e.salary) from Employee e");
    Double result=(Double) query1.getSingleResult();
    System.out.println("Max Employee Salary :"+result);*/
//        Cep cep = new Cep();
//        List<Cep> list = new ArrayList<>();
//        HttpClientBuilder builder = HttpClientBuilder.create();
//        JSONObject json;
//        json = json("85805410", builder);
//        cep.setCep(json.getString("cep"));
//        cep.setLogradouro(json.getString("logradouro"));
//        cep.setComplemento(json.getString("complemento"));
//        cep.setBairro(json.getString("bairro"));
//        cep.setLocalidade(json.getString("localidade"));
//        cep.setUf(json.getString("uf"));
//        cep.setUnidade(json.getString("unidade"));
////        cep.setIbge(json.getString("ibge"));
//        System.out.println(json);
               String path = new File("").getCanonicalPath();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document doc = docBuilder.parse(path+"/arquivo.xml");
            
            NodeList listPeople = doc.getElementsByTagName("pessoa");
            int sizeList = listPeople.getLength();
            for (int i = 0; i < sizeList; i++) {
                Node noPeople = listPeople.item(i);
                if(noPeople.getNodeType() == Node.ELEMENT_NODE){
                    Element elementPeople = (Element) noPeople;
                    String id = elementPeople.getAttribute("id");
                    System.out.println("Id is: "+id);
                     NodeList listChildPeople = elementPeople.getChildNodes();
                     int sizeListchild = listChildPeople.getLength();
                     for (int j = 0; j < sizeListchild; j++) {
                        Node noChildren = listChildPeople.item(j);
                        if(noChildren.getNodeType() == Node.ELEMENT_NODE){
                            Element elementChildren = (Element) noChildren;
                            switch(elementChildren.getTagName()){
                                case "nome":
                                    System.out.println("Name is: "+elementChildren.getTextContent());
                                    break;
                                case "idade":
                                    System.out.println("Idade is: "+elementChildren.getTextContent());
                                    break;
                                case "peso":
                                    System.out.println("Weigth is: "+elementChildren.getTextContent());
                                    break;
                                default:
                                    System.out.println("Error "+ i+j);
                                    break;
                            }
                        }
                    }
                }
            }
            
            
    }
    private static JSONObject json(String url, HttpClientBuilder builder){
        HttpGet httpget = new HttpGet("https://viacep.com.br/ws/"+url+"/json/");
        HttpResponse response;
        String returnResponse;
        try {
            response = builder.build().execute(httpget);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        try {
            returnResponse = EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            return null;
        }
        JSONObject o = new JSONObject(returnResponse);
        return o;
    }
}
